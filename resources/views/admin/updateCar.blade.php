@extends('layouts.app')

@section('content')

<div class="container">
  @if(Session::has('success'))
     
    <div class="alert alert-success" id="success" role="alert">
        {{Session::get('success')}}
    </div>
  @elseif(Session::has('error'))
    <div class="alert alert-danger" id="error" role="alert">
      {{Session::get('error')}}
    </div>
  @endif
  @if(count($errors->all())>0)
  <div class="alert alert-danger" id="success" role="alert">
    @foreach ($errors->all() as $message) 
        {{$message}}
    @endforeach
    
  </div>
  @endif
    <div class="col-md-6 offset-md-3">
        <form action={{route('updateCar')}} method="POST"  enctype="multipart/form-data">

            @csrf
                <div class="form-group">
                  <label for="brand">Brand</label>
                <input type="text" class="form-control" id="brand" name="brand" placeholder="Enter Brand" value="{{$car->brand}}">
                </div>
                <div class="form-group">
                  <label for="model">Model</label>
                  <input type="text" class="form-control" id="model" name="model" placeholder="Enter Model" value="{{$car->model}}">
                </div>
                
                    <div class="form-group">
                        <label for="fuel">Select fuel</label>
                        <select class="form-control" id="fuel" name="fuel">
                          <option value="Diesel" @if($car->fuel == "Diesel") selected @endif>Disel</option>
                          <option value="Petrol"  @if($car->fuel == "Petrol") selected @endif>Petrol</option>
                        </select>
                      </div>
               
                
                <div class="form-group">
                  <label for="year">Year of production</label>
                <input type="number" min="1000" max="2020" class="form-control" id="year" name="year" placeholder="Enter year of production" value="{{$car->year}}">
                </div>
                <div class="form-group">
                  <label for="price">Price per day</label>
                  <input type="number" class="form-control" id="price" name="price" placeholder="Enter Price per day" value="{{$car->price_per_day}}">
                </div>
                <div class="form-group">
                  <label for="exampleFormControlFile1">Upload image</label>
                  <input type="file" class="form-control-file" id="image" name="image">
                </div>

                <input type="hidden" value="{{$car->id}}" name="id">
                <input type="hidden" name="_method" value="put" />
                <button type="submit">Update</button>

        </form>


    </div>



</div>






@endsection