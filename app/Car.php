<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table = 'cars';

    protected $fillable = [
        'brand', 
        'model', 
        'year',
        'fuel',
        'price_per_day',
        'location_id',
    ];

    public function location(){
        return $this->belongsTo('App\Location');
    }

    public function users(){
        return $this->belongsToMany('App\User')
                    ->withPivot('start_date', 'end_date')
                    ->withTimestamps();
    }

}
