<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreLocation;
use App\Location;
use App\City;
use FarhanWazir\GoogleMaps\GMaps;
use Geocoder;


use Auth;

class LocationController extends Controller
{
    public function createRentShop(){
        $center = '41.6086, 21.7453';
        $config['center'] = $center;
        $config['zoom'] = '8';
        $config['map_height'] = '400px';

        $gmap = new GMaps();
        $gmap->initialize($config);
       

       /*   $marker['position'] = $coordinates;
        $marker['infowindow_content'] = 'Bataljon Stiv Naumov';
        $gmap->add_marker($marker); */


     
        $map = $gmap->create_map();

        $city = City::all();
        return view('admin.createRentShop')->with(['city'=>$city, 'map' => $map]); 
    }

    public function storeLocation(StoreLocation $request){
        $user_id = Auth::id();
        $coordinates = Geocoder::getCoordinatesForAddress($request->address);
        /* dd($coordinates); */
        if($coordinates['lng'] > 0){
            
            $lng = $coordinates['lng'];
            $lat = $coordinates['lat']; 
        
        
            if($user_id){
                $location = Location::create([
                    'email' =>$request->email,
                    'name' => $request->location,
                    'address' => $request->address,
                    'phone_number' => $request->phone_number,
                    'description' => $request->description,
                    'user_id' => $user_id,
                    'latitude' => $lat,
                    'longitued' => $lng
                ]);
            
                if($location){
                    return redirect()->route('createCar');
                }
            }
       
        }else{
           
            return redirect()->route('createRentShop')->with('error', 'Please enter your address like "Street 5, City"');
    
        }
    }

    public function viewCompany(){
        $location  = Location::find(Auth::user()->location->id);

        return view('admin.viewCompany')->with(['location' => $location]);
    }

    public function viewAllCompanies(){
        $companies = Location::all();

        return view('viewAllCompanies')->with(['companies'=>$companies]);
    }

    public function updateLocation($id){
        $location = Location::find($id);

        return view('admin.updateCompany')->with(['location' => $location]);
    }

    public function updateCompanyInDB(StoreLocation $request){
        $coordinates = Geocoder::getCoordinatesForAddress($request->address);
        /* dd($coordinates); */
        if($coordinates['lng'] > 0){
            
            $lng = $coordinates['lng'];
            $lat = $coordinates['lat']; 
            $location = Location::find($request->id);
            $location->name = $request->location;
            $location->email = $request->email;
            $location->address = $request->address;
            $location->phone_number = $request->phone_number;
            $location->description = $request->description;
            $location->latitude = $lat;
            $location->longitued = $lng;
            $saved = $location->save();
       

        if($saved){
            return back()->with('success', 'Company updated successfully');
        }else{
            return back()->with('error', 'Something went wrong. Try again');
        }
    }else{
        return back()->with('error', 'Please enter your address like "Street 5, City"');
    }

    }
}
