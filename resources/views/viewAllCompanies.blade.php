@extends('layouts.app')

@section('content')

<div class="container">

    
    <div class="row">
        

    @if(count($companies)>0)
        @foreach ($companies as $company)
            <div class="col-lg-3 col-md-6">
                <div class="card" style="width: 15rem;">
               
                <div class="card-body">
                    <h5 class="card-title">{{$company->name}}</h5>
                    <p class="card-text">Address: {{$company->address}}</p>
                </div>
                <ul class="list-group list-group-flush">
                <li class="list-group-item">Own cars: {{count($company->cars)}}</li>
                
                </ul>
                <div class="card-body">
                <a href="{{route('viewCarCompanies', ['id' => $company->id])}}" class="card-link">View Company Cars</a>
            
                </div>
            </div>
        </div>
        @endforeach
   
    @endif

    </div>

</div>

@endsection