<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCar;
use App\Car;
use Auth;
use App\Location;
use Image;
use FarhanWazir\GoogleMaps\GMaps;
use App\City;



class CarController extends Controller
{
    public function createCar(){

        return view('admin.createCars');
    }

    public function storeCar(StoreCar $request){
       
        $car = Car::create([
            'model' => $request->model,
            'brand' => $request->brand,
            'year' => $request->year,
            'fuel' => $request->fuel,
            'price_per_day' => $request->price,
            
            'location_id' => Auth::user()->location->id
        ]);

        if(isset($request->image)){
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/images/';
            $originalPath = public_path().'/images/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
            $thumbnailImage->resize(200,200);
            $thumbnailImage->save($thumbnailPath.time().$originalImage->getClientOriginalName()); 
            $car->image=time().$originalImage->getClientOriginalName();
            $car->save();
        }

        if($car){
            return back()->with('success','Car add successfully');
        }else{
            return back()->with('error','Something went wrong please try again');

        }
    }


    public function viewAllCars(){
        $cars = Car::where('location_id', Auth::user()->location->id)->get();

        return view('admin.viewAllCars')->with(['cars'=> $cars]);
    }

    public function viewCar($id){
        $car = Car::find($id);

        return view('admin.updateCar')->with(['car'=>$car]);
    }

    public function updateCar(StoreCar $request){
      
        $car = Car::find($request->id);

        if(isset($request->image)){
            $originalImage= $request->file('image');
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/images/';
            $originalPath = public_path().'/images/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
            $thumbnailImage->resize(200,200);
            $thumbnailImage->save($thumbnailPath.time().$originalImage->getClientOriginalName()); 
            $car->image=time().$originalImage->getClientOriginalName();
            $car->save();
        }
        if($car){
            $car->model = $request->model;
            $car->brand = $request->brand;
            $car->year = $request->year;
            $car->fuel = $request->fuel;
            $car->price_per_day = $request->price;

            $car->save();
            return redirect()->route('viewAllCars')->with('success','Car updated successfully');
        }else{
            return redirect()->back()->with('error','Car updated successfully');
        }
    }

    public function viewMyRentedCars(){
        $cars = Car::where('location_id',  Auth::user()->location->id )
        ->join('car_user', 'car_id', '=', 'cars.id')
        ->join('users', 'users.id', '=', 'car_user.user_id')
       
        ->select('cars.*', 'car_user.start_date', 'car_user.end_date', 'users.first_name', 'users.last_name', 'users.email', 'users.phone_number')
        ->get();

        return view('admin.viewRentedCars')->with(['cars'=>$cars]);

    }

    public function deleteCar($id){
        $car = Car::findOrFail($id)->delete();
        
        if($car){
            $data = [
                'status'=>'deleted',
                
                
            ];
            return response()->json($data);
        }
    }


    public function viewAllCarsFront(Request $request){
        /* dd($request); */
        $cars = Car::all();
        $locations = Location::all();
        $cities = City::all();
        $center = null;
        $map = $this->setMap($center);

        if($request->from!=null && $request->to != null){

        }else{

            return view('viewAllCars')->with(['cars'=>$cars, 'location'=>$locations, 'map' => $map, 'cities' => $cities]);
        }
    }

    public function setMap($center){
        if($center == null){
            $center = '41.6086, 21.7453';
        }
        $locations = Location::all();
        
        $config['center'] = $center;
        $config['zoom'] = '8';
        $config['map_height'] = '400px';

        $gmap = new GMaps();
        $gmap->initialize($config);
       
        //dd($locations);
        foreach($locations as $location){
            $coordinates = $location->latitude .",". $location->longitued;
            
            $marker['position'] = "$coordinates";
            $marker['infowindow_content'] = $location->name;
            $gmap->add_marker($marker);
        }

     
        $map = $gmap->create_map();
        return $map;
    }

    public function viewCarFront($id){
        $car = Car::find($id);
        
        $otherCars = Car::where('brand', '=' ,$car->brand)->where('id', '!=', $car->id)->get();
        return view('viewCar')->with(['car'=>$car, 'otherCars'=>$otherCars]);


    }


    public function rentCar(Request $request){
       $cars = Car::find($request->id);
        $cars->users()->attach(Auth::user()->id, ['start_date'=>$request->from, 'end_date'=>$request->to]);
        
            return redirect()->route('viewRentedCars')->with('success','You successfully rented the car');
        
    }

    public function viewRentedCars(){

        $cars = Car::join('car_user', 'car_id', '=', 'cars.id')
                    ->where('car_user.user_id', Auth::user()->id)
                    ->select('cars.*', 'car_user.start_date', 'car_user.end_date')
                    ->get();


      return view('viewRentedCars')->with(['cars' => $cars]);
    }

    public function deleteRentalContract($id){
        $cars = Car::find($id);
        $addPivot = $cars->users()->detach();
       $data = [
                'status'=>'deleted',
                
                
            ];
            return response()->json($data);
        
    }

    public function filterCars(Request $request){
       
        $from = $request->from;
        $to = $request->to;
        $carsExist = \DB::table('car_user')->pluck('car_id')->all();
       
        if(isset($request->company_id)){
        $cars = Car::where('location_id', $request->company_id)
                    ->leftJoin('car_user', 'car_id', '=', 'cars.id')
                    ->whereNotBetween('car_user.start_date', [$from, $to] )
                    ->whereNotBetween('car_user.end_date', [$from, $to] ) 
                    ->select('cars.*')
                    ->get();

            $carsNew = Car::where('location_id', $request->company_id)->whereNotIn('cars.id', $carsExist)->get();
        }else{
            $cars = Car::leftJoin('car_user', 'car_id', '=', 'cars.id')
                        ->whereNotBetween('car_user.start_date', [$from, $to] )
                        ->whereNotBetween('car_user.end_date', [$from, $to] ) 
                        ->select('cars.*')
                        ->get();

            $carsNew = Car::whereNotIn('cars.id', $carsExist)->get();

                  
        }
        
        $carsMerged = $cars->merge($carsNew);  
        
        return $carsMerged;
    }

    

    public function viewCarCompanies($id){
        $cars = Car::where('location_id', $id)->get();

        return view('viewCompanyCar')->with(['cars'=>$cars]);
    }
}
