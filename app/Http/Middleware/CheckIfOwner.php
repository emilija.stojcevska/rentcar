<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIfOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->is_admin != 1){
                return redirect('/');
            }
        }else{
            return redirect('/login');
        }
        return $next($request);
    }
}
