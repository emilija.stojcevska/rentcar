@extends('layouts.app')

@section('content')

    <div class="container">
        @if(Session::has('success'))
     
            <div class="alert alert-success" id="success" role="alert">
                {{Session::get('success')}}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-danger" id="error" role="alert">
            {{Session::get('error')}}
            </div>
        @endif
      @if(count($errors->all())>0)
      <div class="alert alert-danger" id="success" role="alert">
          @foreach ($errors->all() as $message) 
              {{$message}}
          @endforeach
          
      </div>
      @endif
        <div class="col-md-4">
            <form action="{{route('updateCompanyInDB')}}" method="POST">
                @csrf
                <div class="form-group">
                  <label for="shop_name">Name of your shop</label>
                <input type="text" class="form-control" id="shop_name" value="{{$location->name}}" name="location" placeholder="Enter name" value="{{old('shop_name')}}">
                </div>
                <div class="form-group">
                  <label for="email">Contact email</label>
                  <input type="email" class="form-control" id="email" value="{{$location->email}}" name="email" placeholder="Enter contact email"  value="{{old('email')}}">
                </div>
                <div class="form-group">
                  <label for="address">Enter address</label>
                  <input type="text" class="form-control" id="address" value="{{$location->address}}" name="address" placeholder="Enter address"  value="{{old('address')}}">
                  
                </div>
                <div class="form-group">
                  <label for="phone_number">Enter phone number</label>
                  <input type="text" class="form-control" id="phone_number" value="{{$location->phone_number}}" name="phone_number" placeholder="Enter phone_number"  value="{{old('phone_number')}}">
                  
                </div>
                
                
                <div class="form-group">
                  <label for="description">Description</label>
                  <textarea class="form-control" id="description" name="description" rows="3"  value="{{old('description')}}">
                    {{$location->description}}
                </textarea>
                </div>
                <input type="hidden" value="{{$location->id}}" name="id">
                <input type="hidden" name="_method" value="put" />
                <button type="submit">Update</button>
              </form>
        </div>
        <div class="col-md-8">
            
            <div id="map"></div>


        </div>

    </div>
    


@endsection
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXsGTrx8-JWpbwSv1IIVF0thJkRbZB-zA&callback=initMap&libraries=places">
</script>

<script>
    // Initialize and add the map
    function initMap() {
      // The location of Uluru
      let lat = 41.6086;
      let lng =  21.7453;
      var uluru = {lat: lat, lng: lng};
      // The map, centered at Uluru
      var map = new google.maps.Map(
          document.getElementById('map'), {zoom: 8, center: uluru});
          var input = document.getElementById('pac-input');
          console.log(input);
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
    }
        </script>