<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Car;

class Location extends Model
{
    protected $table = 'locations';

    protected $fillable = [
        'email', 
        'name', 
        'address',
        'latitude',
        'longitued',
        'phone_number',
        'user_id',
        'description'
    ];

    public function cars(){
        return $this->hasMany('App\Car');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
