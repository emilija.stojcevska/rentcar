@extends('layouts.app')

@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-12 map">
            {!! $map['html'] !!}  
        </div>
    </div>
    @if(Auth::check())
    <div class="row">
        
       
        <div class="col-md-6">
            <div class="form-row">
                <div class="col-md-6 mb-3">
                    <label for="from">From</label>
                    <input type="date" class="form-control"  name="from" id="from"  required>
                    <div class="valid-feedback">
                      Looks good!
                    </div>
                  </div>
                <div class="col-md-6 mb-3">
                    <label for="to">To</label>
                    <input type="date" class="form-control"  name="to" id="to"  value="Otto" required>
                    <div class="valid-feedback">
                      Looks good!
                    </div>
                  </div>
              
            </div>

        </div>
    </div>
    @endif

    <div class="row cars">
        

        @if(count($cars)>0)
            @foreach ($cars as $car)
            <div class="col-lg-3 col-md-6">
                <div class="card" style="width: 15rem;">
                
                <img src="{{url('/images/'.$car->image)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$car->model}}</h5>
                        <p class="card-text">{{$car->brand}}</p>
                    </div>
                    <ul class="list-group list-group-flush">
                    <li class="list-group-item">{{$car->year}}</li>
                    <li class="list-group-item">{{$car->price_per_day}}</li>
                    <li class="list-group-item">{{$car->fuel}}</li>
                    <li class="list-group-item">{{$car->location->name}}</li>
                    </ul>
                    <div class="card-body">
                    <a href="{{route('viewCarFront', ['id' => $car->id])}}" class="card-link">View Car</a>
                
                    </div>
                </div>

            </div>
            @endforeach
            @else
            <h5>Rent your car</h5>
    @endif

    </div>

</div>

<script>
    $(document).ready(function(){
       
        $('#from').on('change', function(){
            let from = $('#from').val();
            let to = $('#to').val();
            filter(from, to);
        })
        $('#to').on('change', function(){
            let from = $('#from').val();
            let to = $('#to').val();
            filter(from, to);
        })

        $('.city').on('change', function(){
            console.log(this); 
            let city_id = $(this).val();
            filterByCity(city_id);
        })
    })


    

    function filter(from, to){
        console.log(from);
        $.ajax(
            {
              url: "{{route('filterCars')}}",
              type: 'GET',
              data: {
                            "from":from,
                            "to": to,
                        },
                        success: function (data){
                          console.log(data.length)
                            if(data.length > 0){
                                $('.cars').empty();
                                console.log(data);
                                for(let i =0; i<data.length; i++){
                                   
                                    let card = `
                                    <div class="col-lg-3 col-md-6">
                                        <div class="card" style="width: 15rem;">
                                        <img src="{{url('/images/')}}/${data[i].image}"  class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h5 class="card-title">${data[i].model}</h5>
                                            <p class="card-text">${data[i].brand}</p>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                        <li class="list-group-item">${data[i].year}</li>
                                        <li class="list-group-item">${data[i].price_per_day}</li>
                                        <li class="list-group-item">${data[i].fuel}</li>
                                        </ul>
                                        <div class="card-body">
                                            <a href="view-car/${data[i].id}" class="card-link">View Car</a>
                                        </div>
                                    </div>
                                    </div>
                                    
                                    
                                    
                                    `

                                    $('.cars').append(card);
                                }
                            }
                            
                        }
                            
                        
                    });
    }

</script>




@endsection


