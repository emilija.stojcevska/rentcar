@extends('layouts.app')

@section('content')

<div class="container">

    <div class="col-md-6 offset-md-3">
      @if(Session::has('success'))
     
          <div class="alert alert-success" id="success" role="alert">
              {{Session::get('success')}}
          </div>
      @elseif(Session::has('error'))
          <div class="alert alert-danger" id="error" role="alert">
            {{Session::get('error')}}
          </div>
      @endif
      @if(count($errors->all())>0)
      <div class="alert alert-danger" id="success" role="alert">
          @foreach ($errors->all() as $message) 
              {{$message}}
          @endforeach
          
      </div>
      @endif
        <form action={{route('storeCar')}} method="POST" enctype="multipart/form-data">

            @csrf
                <div class="form-group">
                  <label for="brand">Brand</label>
                <input type="text" class="form-control" id="brand" name="brand" placeholder="Enter Brand" value="{{old('brand')}}">
                </div>
                <div class="form-group">
                  <label for="model">Model</label>
                  <input type="text" class="form-control" id="model" name="model" placeholder="Enter Model" value="{{old('model')}}">
                </div>
                
                    <div class="form-group">
                        <label for="fuel">Select fuel</label>
                        <select class="form-control" id="fuel" name="fuel">
                          <option value="Diesel"  >Disel</option>
                          <option value="Petrol" >Petrol</option>
                        </select>
                      </div>
               
                
                <div class="form-group">
                  <label for="year">Year of production</label>
                  <input type="number" min="1000" max="2020" class="form-control" id="year" value="{{old('year')}}" name="year" placeholder="Enter year of production">
                </div>
                <div class="form-group">
                  <label for="price">Price per day</label>
                  <input type="number" class="form-control" id="price" name="price" placeholder="Enter Price per day" value="{{old('price')}}">
                </div>

                <div class="form-group">
                  <label for="exampleFormControlFile1">Upload image</label>
                  <input type="file" class="form-control-file" id="image" name="image">
                </div>

                <button type="submit">Create</button>

        </form>


    </div>



</div>






@endsection