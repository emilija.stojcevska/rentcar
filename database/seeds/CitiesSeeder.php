<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;
use App\City;
class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = File::get(public_path('maps/mk.json'));
        $cities = json_decode($json);
        
        foreach($cities as $city){
            $city_new = new City;
            $city_new->create([
                'city' => $city->city,
                'lat' => $city->lat,
                'lng' => $city->lng,
            ]);
        }
    }
}
