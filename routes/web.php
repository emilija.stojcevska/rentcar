<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* ss */
Route::get('/', 'CarController@viewAllCarsFront')->name('viewAllCarsFront');


Route::group(['middleware' => ['checkIfOwner']], function () { 
Route::get('create-rent-shop', 'LocationController@createRentShop')->name('createRentShop');
Route::post('store-location', 'LocationController@storeLocation')->name('storeLocation');
Route::get('view-my-company', 'LocationController@viewCompany')->name('viewCompany');
Route::get('update-location/{id}', 'LocationController@updateLocation')->name('updateLocation');
Route::put('updateCompanyInDB', 'LocationController@updateCompanyInDB')->name('updateCompanyInDB');


Route::get('view-my-cars', 'CarController@viewAllCars')->name('viewAllCars');
Route::get('view-my-rented-cars', 'CarController@viewMyRentedCars')->name('viewMyRentedCars');
Route::get('create-car', 'CarController@createCar')->name('createCar');
Route::post('store-car', 'CarController@storeCar')->name('storeCar');
Route::get('view-my-car/{id}', 'CarController@viewCar')->name('viewCar');
Route::put('update-car', 'CarController@updateCar')->name('updateCar');
Route::delete('delete-car/{id}', 'CarController@deleteCar')->name('deleteCar');

});


Route::group(['middleware' => ['CheckIfRenter']], function () { 

Route::get('view-car/{id}', 'CarController@viewCarFront')->name('viewCarFront');

Route::get('view-companies', 'LocationController@viewAllCompanies')->name('viewAllCompanies');
Route::post('rent-car', 'CarController@rentCar')->name('rentCar');
Route::get('viewRentedCars', 'CarController@viewRentedCars')->name('viewRentedCars');

Route::delete('deleteRentalContract/{id}', 'CarController@deleteRentalContract')->name('deleteRentalContract');
Route::get('filterCars', 'CarController@filterCars')->name('filterCars');


Route::get('viewCarCompanies/{id}', 'CarController@viewCarCompanies')->name('viewCarCompanies');

});

Auth::routes(); 
Route::get('/home', 'HomeController@index')->name('home');
