@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        @if(count($errors->all())>0)
      <div class="alert alert-danger" id="success" role="alert">
          @foreach ($errors->all() as $message) 
              {{$message}}
          @endforeach
          
      </div>
      @endif
        <div class="col-md-6">
            
                <div>Rent this car</div>
                
                
            
            <form action="{{route('rentCar')}}" method="POST">
                @csrf
                <input type="hidden" value="{{$car->id}}" name="id">
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="from">From</label>
                        <input type="date" class="form-control"  name="from" id="from"  required>
                        <div class="valid-feedback">
                          Looks good!
                        </div>
                      </div>
                    <div class="col-md-6 mb-3">
                        <label for="to">To</label>
                        <input type="date" class="form-control"  name="to" id="to"  required>
                        <div class="valid-feedback">
                          Looks good!
                        </div>
                      </div>
                  
                </div>
                <button type="submit">Rent this car</button>
                </form>

        </div>
        <div class="col-md-4 offset-md-2">
            <h4>Model: {{$car->model}}</h4>
            <h4>Brand: {{$car->brand}}</h4>
            <h4>Price per day: {{$car->price_per_day}}</h4>
            <h4>Fuel: {{$car->fuel}}</h4>
            <h4>Company: {{$car->location->name}}</h4>
            <h4>Location: {{$car->location->address}}</h4>
        </div>

    </div>

    


    <div class="row">
        @if(count($otherCars)>0)
        <div class="col-md-12">
        <h5> Similar cars </h5>
        </div>
        @foreach ($otherCars as $car)
        <div class="col-lg-3 col-md-6">
            <div class="card" style="width: 15rem;">
                <img src="{{url('/images/'.$car->image)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{$car->model}}</h5>
                    <p class="card-text">{{$car->brand}}</p>
                </div>
                <ul class="list-group list-group-flush">
                <li class="list-group-item">{{$car->year}}</li>
                <li class="list-group-item">{{$car->price_per_day}}</li>
                <li class="list-group-item">{{$car->fuel}}</li>
                <li class="list-group-item">{{$car->location->name}}</li>
                </ul>
                <div class="card-body">
                <a href="{{route('viewCarFront', ['id' => $car->id])}}" class="card-link">View Car</a>
               
                </div>
            </div>
        </div>
        @endforeach
        @endif

    </div>

</div>



@endsection


