@extends('layouts.app')

@section('content')

    <div class="container">
      @if(Session::has('error'))
      <div class="alert alert-danger" id="error" role="alert">
        {{Session::get('error')}}
      </div>
    @endif
      @if(count($errors->all())>0)
      <div class="alert alert-danger" id="success" role="alert">
          @foreach ($errors->all() as $message) 
              {{$message}}
          @endforeach
          
      </div>
      @endif
        <div class="col-md-4">
            <form action="{{route('storeLocation')}}" method="POST">
                @csrf
                <div class="form-group">
                  <label for="shop_name">Name of your shop</label>
                <input type="text" class="form-control" id="shop_name" name="location" placeholder="Enter name" value="{{old('shop_name')}}">
                </div>
                <div class="form-group">
                  <label for="email">Contact email</label>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Enter contact email"  value="{{old('email')}}">
                </div>
                <div class="form-group">
                  <label for="address">Enter address</label>
                  <input type="text" class="form-control" id="address" name="address" placeholder="Bataljon Stiv Naumov 5, Bitolax`"  value="{{old('address')}}">
                  
                </div>
                <div class="form-group">
                  <label for="phone_number">Enter phone number</label>
                  <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Enter phone_number"  value="{{old('phone_number')}}">
                  
                </div>
                
                
                <div class="form-group">
                  <label for="description">Description</label>
                  <textarea class="form-control" id="description" name="description" rows="3"  value="{{old('description')}}"></textarea>
                </div>

                <button type="submit">Create</button>
              </form>
        </div>
        <div class="col-md-8">
            
          {!! $map['html'] !!}  


        </div>

    </div>
    


@endsection
