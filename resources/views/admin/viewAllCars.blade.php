@extends('layouts.app')

@section('content')

<div class="container">
    @if(Session::has('success'))
        <div class="col-md-12">
        <div class="alert alert-success" id="success" role="alert">
            {{Session::get('success')}}
        </div>
        </div>
    @elseif(Session::has('error'))
        <div class="col-md-12">
            <div class="alert alert-danger" id="error" role="alert">
            {{Session::get('error')}}
            </div>
        </div>
    @endif
    <div class="row">
     

        @if(count($cars)>0)
    @foreach ($cars as $car)
    <div class="col-lg-3 col-md-6">
        <div class="card" style="width: 15rem;">
            <img src="{{url('/images/'.$car->image)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{$car->model}}</h5>
                <p class="card-text">{{$car->brand}}</p>
            </div>
            <ul class="list-group list-group-flush">
            <li class="list-group-item">{{$car->year}}</li>
            <li class="list-group-item">{{$car->price_per_day}}</li>
            <li class="list-group-item">{{$car->fuel}}</li>
            </ul>
            <div class="card-body">
            <a href="{{route('viewCar', ['id' => $car->id])}}" class="card-link">Update</a>
            <button  class="card-link delete" data-id="{{$car->id}}">Delete</button>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <h5>You dont have any cars</h5>
    
    <a href="{{route('createCar')}}"> Create car</a>
    @endif

    </div>

</div>

<script>

    $(document).ready(function(){
        $('.delete').on('click', function(){
            var x = confirm("Are you sure you want to delete?");
            if (x){
                let id = $(this).attr('data-id');
               let card = $(this);
                $.ajax(
                    {
                        url: "delete-car/"+id,
                        type: 'DELETE',
                        data: {
                            "id":id,
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (data){
                            if(data.status == 'deleted'){
                                $(card).parent().parent().remove(); 
                            }
                            
                        }
                    });
            }else{
                return false;
            }
        })
    })

</script>

@endsection


